
#define API_DISTURL "http://kztimerglobal.com/api/v1.0/record_filters/distributions?stages=0&tickrates=128&has_teleports=false&map_ids=%i&mode_ids=%i"
// https://kztimerglobal.com/api/v1.0/record_filters/distributions?stages=0&mode_ids=200&tickrates=128&has_teleports=false&limit=500

/*

minPoints = document.getElementById('points').value;

    if (minPoints > 1000) {
      minPoints = Math.round(document.getElementById('points').value/data.length) / 1000;
    }
    else {
      minPoints = document.getElementById('points').value/1000;
    }

    for (i=0;i<data.length;i++) {
      record_filter_id = data[i].record_filter_id;
      c = data[i].c;
      d = data[i].d;
      loc = data[i].loc;
      scale = data[i].scale;
      top_scale = data[i].top_scale;

      for (j=0;j<10000;j++) {
        t = ((j - loc) / scale);
        var percentile = Math.pow(1 + Math.pow(t,c),-d);
        if(percentile < minPoints){
          record_dictionary[record_filter_id] = j;
          break;
        }
      }
      //record_list.push([record_filter_id,i])
    }
    */
// http://kztimerglobal.com/api/v1.0/record_filters/distributions?stages=0&tickrates=128&has_teleports=false&map_ids=223&mode_ids=200
float CalculateExpectedTime(int minPoints, int mode)
{
    float c = gF_C[mode];
    float d = gF_D[mode];
    float loc = gF_Loc[mode];
    float scale = gF_Scale[mode];
    float top_scale = gF_TopScale[mode];

    //PrintToServer("c: %f\nd: %f")


    if (!gB_HasDistribution[mode])
    {
        return -1.0;
    }
    
    if (minPoints > 1000 || minPoints <= 0)
    {
        minPoints = 600;
    }
    float fMin = float(minPoints)/1000;
    float t;
    for (float j = 0.0; j < 10000.0; j++)
    {
        t = ((j - loc) / scale);
        float perc = Pow(1 + Pow(t, c), -d)*top_scale;
        if (perc < fMin)
        {
            return j;
        }
    }

    return -1.0;
}

void FetchDistribution(int mode)
{
    PrintToServer("Fetching distributions for mode %i", mode);
    int modeId = mode == Mode_Vanilla ? 202 : mode == Mode_SimpleKZ ? 201 : 200;

    char url[256];
    Format(url, sizeof(url), API_DISTURL, gI_MapId, modeId);
    PrintToServer("url: %s", url);

    Handle request = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, url);
    SteamWorks_SetHTTPRequestHeaderValue(request, "Accept", "application/json");
    SteamWorks_SetHTTPCallbacks(request, Http_Distribution_Completed, .fData=HTTP_Distribution_DataReceived);
    SteamWorks_SetHTTPRequestContextValue(request, mode);
    SteamWorks_SendHTTPRequest(request);
}

public int Http_Distribution_Completed(Handle hRequest, bool bFailure, bool bRequestSuccessful, EHTTPStatusCode eStatusCode)
{
    PrintToServer("Status code burr: %i - Failure: %i - bRequestSuccess - %i", eStatusCode, bFailure, bRequestSuccessful);
}

public int HTTP_Distribution_DataReceived(Handle request, bool failure, int offset, int bytesReceived, int mode)
{
    if (failure)
    {
        CPrintToChatAll("[{gold}API{default}] No distribution exists or request failed for {red}%s{default} mode", mode == Mode_Vanilla ? "Vanilla" : mode == Mode_SimpleKZ ? "SimpleKZ" : "KZTimer");
    }

    else
    {
        int responseBodySize = 0;
        SteamWorks_GetHTTPResponseBodySize(request, responseBodySize);

        if (responseBodySize <= 2)
        {
            CPrintToChatAll("[{gold}API{default}] No distribution exists for {red}%s{default} mode", mode == Mode_Vanilla ? "Vanilla" : mode == Mode_SimpleKZ ? "SimpleKZ" : "KZTimer");
        }
        else
        {
            SteamWorks_GetHTTPResponseBodyCallback(request, HTTP_Distribution_Data, mode);
        }
    }

    delete request;
}


public int HTTP_Distribution_Data(const char[] response, int mode)
{
    // global api
    JSON_Object hJson = json_decode(response).GetObjectIndexed(0);

    gF_C[mode] = hJson.GetFloat("c");
    gF_D[mode] = hJson.GetFloat("d");
    gF_Loc[mode] = hJson.GetFloat("loc");
    gF_Scale[mode] = hJson.GetFloat("scale");
    gF_TopScale[mode] = hJson.GetFloat("top_scale");

    // Cleanup
    if (hJson != null) hJson.Cleanup();

    delete hJson;

    CPrintToChatAll("[{gold}API{default}] Distribution loaded for {green}%s{default} mode", mode == Mode_Vanilla ? "Vanilla" : mode == Mode_SimpleKZ ? "SimpleKZ" : "KZTimer");
    gB_HasDistribution[mode] = true;
}