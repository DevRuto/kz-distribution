#define PLUGIN_AUTHOR "Ruto"
#define PLUGIN_VERSION "0.02"

// sourcemod
#include <sourcemod>
#include <sdktools>
#include <cstrike>

// third party
#include <colorvariables>
#include <steamworks>
#include <json>
#include <GlobalAPI-Core>
#include <gokz/core>

#pragma dynamic 131072
#pragma newdecls required

int gI_MapId = -1;
bool gB_HasDistribution[MODE_COUNT] = false;
float gF_C[MODE_COUNT];
float gF_D[MODE_COUNT];
float gF_Loc[MODE_COUNT];
float gF_Scale[MODE_COUNT];
float gF_TopScale[MODE_COUNT];

#include "burr.sp"
#include "apimap.sp"

public Plugin myinfo =
{
    name = "KZ - Distributions",
    author = PLUGIN_AUTHOR,
    description = "KZ Distribution fun",
    version = PLUGIN_VERSION,
    url = ""
}

public void OnPluginStart()
{
    RegConsoleCmd("sm_burr", Command_Burr, "!burr <points> - Get expected time for certain points. Default: 600 points if not specified");
    RegConsoleCmd("sm_dists", Command_Dists, "Prints distribution status");
}

public void OnMapStart()
{
    GetMapId();
}

public Action Command_Burr(int client, int args) 
{
    int minPoints = 600;
    int mode = GOKZ_GetCoreOption(client, Option_Mode);

    if (args > 0)
    {
        char argPoints[10];
        GetCmdArg(1, argPoints, sizeof(argPoints));
        minPoints = StringToInt(argPoints);
    }

    float timeSeconds = CalculateExpectedTime(minPoints, mode);

    if (timeSeconds < 0.0) 
    {
        CReplyToCommand(client, "[{gold}API{default}] There is no distribution for {red}%s{default} mode", mode == Mode_Vanilla ? "Vanilla" : mode == Mode_SimpleKZ ? "SimpleKZ" : "KZTimer");
    }
    else 
    {
        int mins = RoundToFloor(timeSeconds / 60.0);
        int seconds = RoundToFloor(timeSeconds - (mins*60.0));
        if (mins == 0)
            CReplyToCommand(client, "[{gold}API{default}] You need to get a pro time within {green}%f{default} seconds to get at least {green}%i{default} points", timeSeconds, minPoints);
        else
            CReplyToCommand(client, "[{gold}API{default}] You need to get a pro time within {green}%i:%i{default} minutes to get at least {green}%i{default} points", mins, seconds, minPoints);
    }

    return Plugin_Handled;
}

public Action Command_Dists(int client, int args)
{
    for (int mode = 0; mode < MODE_COUNT; mode++)
    {
        if (gB_HasDistribution[mode])
        {
            CReplyToCommand(client, "[{gold}API{default}] {green}%s{default} mode: {green}YES", mode == Mode_Vanilla ? "Vanilla" : mode == Mode_SimpleKZ ? "SimpleKZ" : "KZTimer");
        }
        else 
        {
            CReplyToCommand(client, "[{gold}API{default}] {green}%s{default} mode: {red}NO", mode == Mode_Vanilla ? "Vanilla" : mode == Mode_SimpleKZ ? "SimpleKZ" : "KZTimer");
        }
    }
}