
#define API_MAPURL "http://kztimerglobal.com/api/v1.0/maps/name/%s"

void GetMapId()
{
    char mapPath[PLATFORM_MAX_PATH];
    char mapDisplayName[64];

    GetCurrentMap(mapPath, sizeof(mapPath));
    GetMapDisplayName(mapPath, mapDisplayName, sizeof(mapDisplayName));

    char url[256];
    Format(url, sizeof(url), API_MAPURL, mapDisplayName);
    PrintToServer("url: %s", url);

    Handle request = SteamWorks_CreateHTTPRequest(k_EHTTPMethodGET, url);
    SteamWorks_SetHTTPCallbacks(request, .fData=HTTP_Maps_DataReceived);
    SteamWorks_SendHTTPRequest(request);
}

public int HTTP_Maps_DataReceived(Handle request, bool failure, int offset, int bytesReceived)
{
    int responseBodySize = 0;
    SteamWorks_GetHTTPResponseBodySize(request, responseBodySize);

    if (responseBodySize <= 2)
    {
        CPrintToChatAll("[{gold}API{default}] Error getting Map ID");
        PrintToServer("Unable to get Map ID");
    }
    else
    {
        SteamWorks_GetHTTPResponseBodyCallback(request, HTTP_Maps_Data);

        for (int mode = 0; mode < MODE_COUNT; mode++)
        {
            FetchDistribution(mode);
        }
    }

    delete request;
}


public int HTTP_Maps_Data(const char[] response)
{
    // global api
    JSON_Object hJson = json_decode(response);

    gI_MapId = hJson.GetInt("id");

    // Cleanup
    if (hJson != null) hJson.Cleanup();

    delete hJson;
}